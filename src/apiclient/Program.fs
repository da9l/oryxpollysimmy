﻿open System
open System.Net.Http
open System.Text.Json
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
// open Microsoft.Extensions.Http.Polly
open Polly

open FSharp.Control.Tasks.V2.ContextInsensitive

open Oryx
// open Oryx.SystemTextJson.ResponseReader 
open Oryx.ThothJsonNet.ResponseReader
open Thoth.Json.Net

//TODO: Add multiple async calls. How to do that with oryx? 
//     what do we call? Lets take a list of queries and send them to the same endpoint.  
//     Call multiple search engines with same queries. Yaa!
//TODO: log everything that happens - withLogger
//TODO: put output into separate files. - why?
//TODO: Add Simmy to introduce problems in our connection
//TODO: Add Polly to solve problem with circuit breaker.

let wikiClientUri = Uri "https://en.wikipedia.org/w/api.php"
let wikiClient = "WikiClient"
let options = JsonSerializerOptions()

let query term = [
    struct ("action", "opensearch")
    struct ("search", term)
]


type WikiDataItem = 
    | SearchTerm of string
    | Hits of string list

type WikiDataItems = WikiDataItems of WikiDataItem list

let wikiDataItemDecoder : Decoder<WikiDataItem> =
    Decode.oneOf [
        Decode.string |> Decode.map SearchTerm
        Decode.list Decode.string |> Decode.map Hits
    ]

let wikiDataItemsDecoders : Decoder<WikiDataItems> =
    Decode.list wikiDataItemDecoder
    |> Decode.map WikiDataItems

let queryWikipedia term  = 
    GET
    >=> withUrl wikiClientUri.AbsoluteUri
    >=> withQuery (query term)
    >=> fetch
    >=> json wikiDataItemsDecoders

open System.Threading.Tasks

type IMyClient =
    abstract member GetPage : unit -> Task<WikiDataItems>

type MyClient(clientFactory : IHttpClientFactory) =
    interface IMyClient with
        member __.GetPage() =
            task {
                let ctx =
                    Context.defaultContext
                    |> Context.withHttpClientFactory clientFactory.CreateClient
            
                let! result = queryWikipedia "F#" |> runAsync ctx 
                return 
                    match result with
                    | Ok content -> content
                    | Error error -> failwithf "Some error occured %A" error
            }

let buildHost (argv: string array) =
    let configureAppConfiguration (context: HostBuilderContext) (config: IConfigurationBuilder) =
        config
            .AddEnvironmentVariables()
            .AddCommandLine(argv)
            |> ignore

    let configureLogging (context: HostBuilderContext) (config: ILoggingBuilder) =
        config
            .AddConsole
            |> ignore
    let wikiClient (client:HttpClient) =
        client.BaseAddress <- wikiClientUri
       

            

    let configureServices (services:IServiceCollection) =
        services
            .AddTransient<IMyClient,MyClient>()
            .AddHttpClient( "WikiClient", wikiClient) // TODO: Understand why this is not a fully fluent api. Why does AddHttpClient not return the service?

        |> ignore

    

    HostBuilder()
        .ConfigureAppConfiguration(configureAppConfiguration)
        .ConfigureLogging(configureLogging)
        .ConfigureServices(configureServices)
        .UseConsoleLifetime()
        .Build()

//To run command line client as IHost
//we follow this: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/http-requests?view=aspnetcore-3.1#use-ihttpclientfactory-in-a-console-app

[<EntryPoint>]
let main argv =
    let host = buildHost argv

    use commandScope = host.Services.CreateScope()
    let services = commandScope.ServiceProvider
    let myClient = services.GetRequiredService<IMyClient>()
    myClient.GetPage()
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> printfn "Page result %A"

    0 // return an integer exit code